from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse

from pydantic import BaseModel, Field

from typing import Optional

from utils.jwt_manager import create_token

from config.database import engine, Base

from middlewares.error_handler import ErrorHandler

from routers.movie import movie_router
from routers.user import user_router


app = FastAPI()
app.title = 'Mi aplicación con FastAPI'
app.version = "0.0.1"

app.add_middleware(ErrorHandler)

app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=5, max_length=30)
    overview: str = Field(min_length=5, max_length=200)
    year: int = Field(le=2022)
    rating: float = Field(ge=1, Le=10)
    category: str = Field(min_length=3, max_length=20)

    class Config:
        schema_extra = {
            "example": {
                "title" : "Mi pelicula",
                "overview" : "Descripcion de la pelicula",
                "year" : 2022,
                "rating": 9.8,
                "category": "Accion"

            }
        }


@app.get('/', tags=['Home'])
def message():
    return HTMLResponse('<h1>hello world</h1>')



