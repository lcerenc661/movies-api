from pydantic import BaseModel, Field
from typing import Optional, List

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=1, max_length=30)
    overview: str = Field(min_length=1, max_length=200)
    year: int = Field(le=2022)
    rating: float = Field(ge=1, Le=10)
    category: str = Field(min_length=3, max_length=20)

    class Config:
        schema_extra = {
            "example": {
                "title" : "Mi pelicula",
                "overview" : "Descripcion de la pelicula",
                "year" : 2022,
                "rating": 9.8,
                "category": "Accion"

            }
        }
