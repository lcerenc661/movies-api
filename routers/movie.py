from fastapi import  Path, Query, APIRouter
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from pydantic import BaseModel, Field

from typing import Optional, List

from config.database import Session

from models.movie import Movie as MovieModel

from services.movie import MovieService

from schemas.movie import Movie


movie_router = APIRouter()


@movie_router.get('/movies', tags=['Movies'], response_model=List[Movie])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags= ['Movies'], response_model= dict, status_code=201)
def post_movies(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code = 201, content={"message": "Se ha registrado la pelicula"})

@movie_router.get('/movies/{id}', tags=['Movies'], response_model=Movie)
def get_movies(id: int = Path(ge = 1, le = 2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['Movies'], response_model=List[Movie], status_code=200)
def get_movies_by_category(category_selected : str = Query(min_length=3, max_length=50)) -> List[Movie]:
    db = Session()
    result = MovieService(db).filter_by_category(category_selected)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No hay resultados con ese genero"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.put('/movies/{id}', tags=['Movies'], response_model= dict)
def update_movie(movie : Movie, id: int = Path(ge = 1, le = 2000)) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No hay resultados con ese genero"})
    MovieService(db).update_movie(id,movie)
    return JSONResponse(status_code=200, content={"message": f"Se ha modificado la peliculacon id {id}"})

@movie_router.delete('/movies/{id}', tags=['Movies'], response_model= dict, status_code = 200)
async def delete_movies(id:int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No hay resultados con ese genero"})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code = 200, content={'message': 'Movie succesfully deleted'})
